/*
 * Hello_Light_ATtiny45.c
 *
 * Created: 6/10/2015 16:59:55
 *  Author: Brandy
 */ 

// ============================================================================

#include <avr/io.h>
#define OWOWOD_PORT		PB2	// OWOWOD Port

// ----------------------------------------------------------------------------

// Freq=1KHz, Rate=9600bps - delay = [22,23,24]
// IMPORTANT: For other frequencies this parameter must change.
#define OWOWOD_DELAY	23	// Delay for each bit
// ----------------------------------------------------------------------------
#include <stdint.h>

// ----------------------------------------------------------------------------

#define USINT2DECASCII_MAX_DIGITS 5

// ----------------------------------------------------------------------------

// NOTE: This implementation is borrowed from the LCDDDD library.
// Original source code at: https://bitbucket.org/boyanov/avr/src/default/lcdddd/src/lcdddd/lcdddd.h

uint8_t usint2decascii(uint16_t num, char* buffer)
{
	const unsigned short powers[] = { 10000u, 1000u, 100u, 10u, 1u }; // The "const unsigned short" combination gives shortest code.
	char digit; // "digit" is stored in a char array, so it should be of type char.
	uint8_t digits = USINT2DECASCII_MAX_DIGITS - 1;
	for (uint8_t pos = 0; pos < 5; pos++) // "pos" is index in array, so should be of type int.
	{
		digit = 0;
		while (num >= powers[pos])
		{
			digit++;
			num -= powers[pos];
		}

		// ---- CHOOSE (1), (2) or (3) ----

		// CHOICE (1) Fixed width, zero padded result.
		/*
		buffer[pos] = digit + '0';	// Convert to ASCII
		*/

		// CHOICE (2) Fixed width, zero padded result, digits offset.
		/*
		buffer[pos] = digit + '0';	// Convert to ASCII
		// Note: Determines the offset of the first significant digit.
		if (digits == -1 && digit != 0) digits = pos;
		// Note: Could be used for variable width, not padded, left aligned result.
		*/

		// CHOICE (3) Fixed width, space (or anything else) padded result, digits offset.
		// Note: Determines the offset of the first significant digit.
		// Note: Could be used for variable width, not padded, left aligned result.
		if (digits == USINT2DECASCII_MAX_DIGITS - 1)
		{
			if (digit == 0)
			{
				if (pos < USINT2DECASCII_MAX_DIGITS - 1)	// Check position, so single "0" will be handled properly.
					digit = -16;	// Use: "-16" for space (' '), "-3" for dash/minus ('-'), "0" for zero ('0'), etc. ...
			}
			else
			{
				digits = pos;
			}
		}
		buffer[pos] = digit + '0';	// Convert to ASCII		
	}	
	// NOTE: The resulting ascii text should not be terminated with '\0' here.
	//       The provided buffer maybe part of a larger text in both directions.	
	return digits;
}

// ----------------------------------------------------------------------------

void owowod_delay(void) {
	for (uint8_t i = OWOWOD_DELAY; i != 0; i--) {
		asm volatile ("nop\n\t");
	}
}

// ----------------------------------------------------------------------------

void owowod_print_char(char c) {
	PORTB &= ~(1 << OWOWOD_PORT);	// Set to LO
	owowod_delay();
	for (uint8_t i = 0; i < 8; i++)
	{
		if (c & 1) {
			PORTB |= (1 << OWOWOD_PORT);	// Set to HI
		} else {
			PORTB &= ~(1 << OWOWOD_PORT);	// Set to LO
		}
		owowod_delay();
		c = (c >> 1);
	}
	PORTB |= (1 << OWOWOD_PORT);	// Set to HI
	owowod_delay();
}

void owowod_print_string(char *s) {
	while (*s) {
		owowod_print_char(*s++);
	}
}

// ----------------------------------------------------------------------------

void owowod_print_numdec(uint16_t num) {
	char buffer[USINT2DECASCII_MAX_DIGITS + 1];
	buffer[USINT2DECASCII_MAX_DIGITS] = '\0';   // Terminate the string.
	uint8_t digits = usint2decascii(num, buffer);
	owowod_print_string(buffer + digits);
}

// ----------------------------------------------------------------------------

void owowod_print_numdecp(uint16_t num) {
	char buffer[USINT2DECASCII_MAX_DIGITS + 1];
	buffer[USINT2DECASCII_MAX_DIGITS] = '\0';   // Terminate the string.
	usint2decascii(num, buffer);
	owowod_print_string(buffer);
}

// ============================================================================
int main(){
	char chr=0;
	//
	// init A/D
	//
	ADMUX = (0 << REFS2) | (0 << REFS1) | (0 << REFS0) // Vcc ref
	| (0 << ADLAR) // right adjust
	| (0 << MUX3) | (0 << MUX2) | (1 << MUX1) | (1 << MUX0); // ADC3
	ADCSRA = (1 << ADEN) // enable
	| (1 << ADPS2) | (1 << ADPS1) | (1 << ADPS0); // prescaler /128
	//
	// main loop
	//
	while (1) {
		//
		// initiate conversion
		//
		ADCSRA |= (1 << ADSC);
		//
		// wait for completion
		//
		while (ADCSRA & (1 << ADSC));
		//
		// send result
		//
		chr = ADCL;
		owowod_print_char(chr);
		chr = ADCH;
		owowod_print_char(chr);
	}
}